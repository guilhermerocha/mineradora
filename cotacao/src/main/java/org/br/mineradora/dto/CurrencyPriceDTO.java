package org.br.mineradora.dto;

import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

@JsonPOJOBuilder(buildMethodName = "createCurrencyPrice", withPrefix = "construct")
public class CurrencyPriceDTO {

    public USDBRL USDBRL;

    public USDBRL getUSDBRL() {
        return USDBRL;
    }

    public void setUSDBRL(USDBRL USDBRL) {
        this.USDBRL = USDBRL;
    }
}
