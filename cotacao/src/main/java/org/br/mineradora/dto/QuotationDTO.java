package org.br.mineradora.dto;

import java.math.BigDecimal;
import java.time.LocalDate;

public class QuotationDTO {

    private LocalDate date;
    private BigDecimal currencyPrice;

    public QuotationDTO() {
    }

    public static QuotationDTO buildNewQuotation(CurrencyPriceDTO currencyPriceInfo) {
        QuotationDTO quotation = new QuotationDTO();
        quotation.setDate(LocalDate.now());
        quotation.setCurrencyPrice(new BigDecimal(currencyPriceInfo.getUSDBRL().getBid()));
        return quotation;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public BigDecimal getCurrencyPrice() {
        return currencyPrice;
    }

    public void setCurrencyPrice(BigDecimal currencyPrice) {
        this.currencyPrice = currencyPrice;
    }
}
