package org.br.mineradora.repository;

import io.quarkus.hibernate.orm.panache.PanacheEntity;
import io.quarkus.hibernate.orm.panache.PanacheRepository;
import jakarta.enterprise.context.ApplicationScoped;
import org.br.mineradora.entity.Quotation;

@ApplicationScoped
public class QuotationRepository implements PanacheRepository<Quotation> {
}
