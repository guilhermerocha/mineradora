package org.br.mineradora.service;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import org.br.mineradora.client.CurrencyPriceClient;
import org.br.mineradora.dto.CurrencyPriceDTO;
import org.br.mineradora.dto.QuotationDTO;
import org.br.mineradora.entity.Quotation;
import org.br.mineradora.message.KafkaEvents;
import org.br.mineradora.repository.QuotationRepository;
import org.eclipse.microprofile.rest.client.inject.RestClient;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

@ApplicationScoped
public class QuotationService {

    @Inject
    @RestClient
    CurrencyPriceClient currencyPriceClient;

    @Inject
    QuotationRepository quotationRepository;

    @Inject
    KafkaEvents kafkaEvents;

    public void getCurrencyPrice(){
        CurrencyPriceDTO currencyPriceInfo = this.currencyPriceClient.getPriceByPair("USD-BRL");

        if(updateCurrentInfoPrice(currencyPriceInfo)){
            kafkaEvents.sendNewKafkaEvent(QuotationDTO.buildNewQuotation(currencyPriceInfo));
        }
    }

    private boolean updateCurrentInfoPrice(CurrencyPriceDTO currencyPriceInfo) {
        BigDecimal currencyPrice = new BigDecimal((currencyPriceInfo.getUSDBRL().getBid()));
        AtomicBoolean updatePrice = new AtomicBoolean(false);

        List<Quotation> quotationList = this.quotationRepository.findAll().list();

        if (quotationList.isEmpty()) {
            this.saveQuotation(currencyPriceInfo);
            updatePrice.set(true);
        } else {
            Quotation lastDollarPrice = quotationList.get(quotationList.size() - 1);

            if (currencyPrice.floatValue() > lastDollarPrice.getCurrencyPrice().floatValue()) {
                updatePrice.set(true);
                this.saveQuotation(currencyPriceInfo);
            }
        }

        return updatePrice.get();
    }

    private void saveQuotation(CurrencyPriceDTO currencyInfo){
        Quotation quotation = new Quotation();

        quotation.setDate(LocalDate.now());
        quotation.setCurrencyPrice(new BigDecimal(currencyInfo.getUSDBRL().getBid()));
        quotation.setPair("USD-BRL");
        this.quotationRepository.persist(quotation);
    }
}
