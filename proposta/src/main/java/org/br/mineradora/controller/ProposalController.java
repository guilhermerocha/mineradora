package org.br.mineradora.controller;

import jakarta.inject.Inject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import org.br.mineradora.dto.ProposalDetailsDTO;
import org.br.mineradora.service.ProposalService;
import org.jboss.logging.annotations.Pos;

@Path("/proposal")
public class ProposalController {

    @Inject
    ProposalService proposalService;

    @GET()
    @Path("/{id}")
    public ProposalDetailsDTO findDetailsProposal(@PathParam("id") long id) {
        return this.proposalService.findFullProposal(id);
    }

    @POST
    public Response createProposal(ProposalDetailsDTO proposal) {
        try {
            this.proposalService.createNewProposal(proposal);
            return Response.ok().build();
        } catch (Exception e) {
            return Response.serverError().build();
        }
    }

    @DELETE
    public Response removeProposal(long id){
        try {
            proposalService.removeProposal(id);
            return Response.ok().build();

        } catch (Exception e) {
            return Response.serverError().build();
        }
    }
}
