package org.br.mineradora.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import org.br.mineradora.dto.ProposalDetailsDTO;

import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
public class Proposal {

    @Id
    @GeneratedValue
    private Long id;

    private String customer;

    @Column(name = "price_tonne")
    private BigDecimal priceTonne;

    private Integer tonnes;

    private String country;

    @Column(name = "proposal_validity_days")
    private Integer proposalValidityDays;

    private LocalDate created;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public BigDecimal getPriceTonne() {
        return priceTonne;
    }

    public void setPriceTonne(BigDecimal priceTonne) {
        this.priceTonne = priceTonne;
    }

    public Integer getTonnes() {
        return tonnes;
    }

    public void setTonnes(Integer tonnes) {
        this.tonnes = tonnes;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Integer getProposalValidityDays() {
        return proposalValidityDays;
    }

    public void setProposalValidityDays(Integer proposalValidityDays) {
        this.proposalValidityDays = proposalValidityDays;
    }

    public LocalDate getCreated() {
        return created;
    }

    public void setCreated(LocalDate created) {
        this.created = created;
    }

    public ProposalDetailsDTO entityToDTO(){
        ProposalDetailsDTO proposalDetailsDTO = new ProposalDetailsDTO();
        proposalDetailsDTO.setProposalId(this.id);
        proposalDetailsDTO.setProposalValidityDays(this.proposalValidityDays);
        proposalDetailsDTO.setCountry(this.country);
        proposalDetailsDTO.setPriceTonne(this.priceTonne);
        proposalDetailsDTO.setTonnes(this.tonnes);
        proposalDetailsDTO.setCustomer(this.customer);
        proposalDetailsDTO.setPriceTonne(this.priceTonne);
        return proposalDetailsDTO;
    }

    public static Proposal dtoToEntity(ProposalDetailsDTO proposalDetailsDTO){
        Proposal proposal = new Proposal();
        proposal.setCreated(LocalDate.now());
        proposal.setProposalValidityDays(proposalDetailsDTO.getProposalValidityDays());
        proposal.setCountry(proposalDetailsDTO.getCountry());
        proposal.setPriceTonne(proposalDetailsDTO.getPriceTonne());
        proposal.setTonnes(proposalDetailsDTO.getTonnes());
        proposal.setCustomer(proposalDetailsDTO.getCustomer());
        proposal.setPriceTonne(proposalDetailsDTO.getPriceTonne());
        return proposal;
    }
}
