package org.br.mineradora.service;

import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import org.br.mineradora.dto.ProposalDTO;
import org.br.mineradora.dto.ProposalDetailsDTO;
import org.br.mineradora.entity.Proposal;
import org.br.mineradora.message.KafkaEvents;
import org.br.mineradora.repository.ProposalRepository;

import java.util.Optional;

public class ProposalServiceImpl implements ProposalService {

    @Inject
    ProposalRepository proposalRepository;

    @Inject
    KafkaEvents kafkaEvents;

    @Override
    public ProposalDetailsDTO findFullProposal(final long proposalId) {
        Proposal proposal = this.proposalRepository.findById(proposalId);
        return proposal.entityToDTO();
    }

    @Override
    public void createNewProposal(ProposalDetailsDTO proposalDetailsDTO) {

        ProposalDTO proposal = buildAndSaveNewProposal(proposalDetailsDTO);
        kafkaEvents.sendNewKafkaEvent(proposal);
    }

    @Transactional
    private ProposalDTO buildAndSaveNewProposal(ProposalDetailsDTO proposalDetailsDTO) {
        try {
            Proposal proposal = Proposal.dtoToEntity(proposalDetailsDTO);
            proposalRepository.persist(proposal);
            return new ProposalDTO(
                    proposalRepository.findByCustomer(proposalDetailsDTO.getCustomer()).get().getId(),
                    proposal.getCustomer(),
                    proposal.getPriceTonne());
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException();
        }
    }

    @Override
    @Transactional
    public void removeProposal(long proposalId) {
        Optional.ofNullable(this.proposalRepository.findById(proposalId))
                .ifPresent(proposal -> this.proposalRepository.delete(proposal));
    }
}
