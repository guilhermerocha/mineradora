package org.br.mineradora.entity;


import jakarta.persistence.*;

import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "opportunity")
public class OpportunityEntity {

    @Id
    @GeneratedValue
    private Long id;

    private Date date;

    private String  customer;

    @Column(name = "proposal_id")
    private Long proposalId;

    @Column(name = "price_tonne")
    private BigDecimal priceTonne;

    @Column(name = "last_currency_quotation")
    private Long lastDollarQuotation;

    public OpportunityEntity() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public Long getProposalId() {
        return proposalId;
    }

    public void setProposalId(Long proposalId) {
        this.proposalId = proposalId;
    }

    public BigDecimal getPriceTonne() {
        return priceTonne;
    }

    public void setPriceTonne(BigDecimal priceTonne) {
        this.priceTonne = priceTonne;
    }

    public Long getLastDollarQuotation() {
        return lastDollarQuotation;
    }

    public void setLastDollarQuotation(Long lastDollarQuotation) {
        this.lastDollarQuotation = lastDollarQuotation;
    }
}
